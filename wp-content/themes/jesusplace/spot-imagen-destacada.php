<div class="spot spot-imagen-destacada box">
	
	<div class="row row-vh">
		<div class="col-xs-12 col-sm-7">
			<div class="content box">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="col-xs-12 col-sm-5">
			<div class="hgroup">
				<div class="box">
					<img src="<?php the_post_thumbnail_url();  ?>" alt="Jesus Place" class="img-responsive img-pmp">
				</div>
				<div class="caption box">
					<?php echo get_the_post_thumbnail_caption(); ?>
				</div>
			</div>
		</div>
	</div>

	



    
</div>