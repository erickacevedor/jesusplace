<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<?php
	$stylesheet_root = get_stylesheet_directory();
	include( $stylesheet_root . '/versionado.php' );
	?>

	<!-- ID -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon/android-icon-192x192.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Products Opengraph -->
	<!-- OG -->
	<meta property="og:description" content="Aquí, podrás encontrarte con Dios, con el susurro de su Espíritu, con la inmensa paz que produce saber que su sabiduría te guía."/>
	<meta property="og:title" content="<?php the_title(); ?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php the_permalink(); ?>"/>
	<meta property="og:site_name" content="Jesus Place Store"/>
	<meta property="og:image" content="<?php echo get_the_post_thumbnail_url(); ?>" />
	


	<?php wp_head(); ?>

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/libs/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css<?=$ver;?>">	

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-90633960-7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-90633960-7');
	</script>


</head>

<body <?php global $is_safari; if ( ! wp_is_mobile() && $is_safari ) {  body_class('is-safari-desktop'); }else{ body_class(); } if ( wp_is_mobile() && $is_safari ) {  body_class('is-safari-mobile'); }else{ body_class(); }   ?>      >
	<?php do_action( 'before' ); ?>


	<div class="nav-main">
		

		<div class="searchbox box">
			<div class="container">
				<div class="row row-vh">

					<!-- Info -->
					<div class="col-xs-12 col-sm-5">
						<div class="contact-info box">
							<p class="contact-info-item">
								
							</p>
							<a class="contact-info-item contact-info-email" href="hola@jesusplacestore.com"><span class="fa fa-envelope"></span>	hola@jesusplacestore.com</a>
							<a class="contact-info-item contact-info-tel" href="tel:573014306299"><span class="fa fa-phone"></span> +57 301 4306299</a>
							<a class="contact-info-item contact-info-tel" href="tel:5756424584"><span class="fa fa-phone"></span> +575 642 4584</a>
						</div>
					</div>
					
					<!-- Search box -->
					<div class="col-xs-12 col-sm-3">
						<div class="search-content box">
							<?php get_product_search_form(); ?>
						</div>
					</div>

					<!-- Cart -->
					<div class="col-xs-12 col-sm-2">
						<div class="cart-content box">

							<?php /*
							<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?>
								
							</a>

							*/ ?>


							<div class="bag">
								<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
									<div class="bag-icon">
										<div class="bag-icon-not">
											<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>
										</div>
										<span class="fa fa-shopping-bag"></span>
									</div>
								</a>
							</div>


						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="navbar-container box">
			<nav class="site-navigation">
				<div class="container">
					<div class="row">
						<div class="site-navigation-inner col-sm-12">
							<div class="navbar navbar-default">
								<div class="navbar-header">
									<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
										<span class="sr-only"><?php _e('Toggle navigation','_tk') ?> </span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

									<!-- Your site title as branding in the menu -->
									<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_black.svg" alt="<?php bloginfo( 'name' ); ?>" class="img-responsive">

									</a>
								</div>

								<!-- The WordPress Menu goes here -->
								<?php wp_nav_menu(
									array(
										'theme_location' 	=> 'primary',
										'depth'             => 2,
										'container'         => 'nav',
										'container_id'      => 'navbar-collapse',
										'container_class'   => 'collapse navbar-collapse',
										'menu_class' 		=> 'nav navbar-nav ',
										'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
										'menu_id'			=> 'main-menu',
										'walker' 			=> new wp_bootstrap_navwalker()
									)
								);
								?>

							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>	