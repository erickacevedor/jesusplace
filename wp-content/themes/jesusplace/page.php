<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header(); ?>



<div class="standard-page box">

	<div style="background-image: url(<?php the_post_thumbnail_url();  ?>)" class="hero nostatic box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="hgroup">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="standard-page-content box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

					<?php endwhile; // end of the loop. ?>

				</div>
			</div>
		</div>
	</div>
</div>






<?php //get_sidebar(); ?>
<?php get_footer(); ?>
