<?php /* Template Name: Nosotros */ ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header(); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/three.js/r69/three.min.js"></script>



<div class="standard-page box">

	<div class="hero nostatic box">
		<div class="slider-container box">
			<div class="logo-top">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-white-part.svg" alt="Jesus Place" class="superlogo img-responsive">
			</div>
			<?php echo do_shortcode('[metaslider id=119]'); ?>
		</div>
	</div>
	<div class="standard-page-content box">
		<div class="intro box">
			<div class="container">
				<?php if ( function_exists( 'icit_spot' ) ) icit_spot( '106' ); ?>
			</div>
		</div>
		<!-- Activate later :)
		<div class="m-v box">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<?php if ( function_exists( 'icit_spot' ) ) icit_spot( '113' ); ?>
					</div>
					<div class="col-xs-12 col-sm-6">
						<?php if ( function_exists( 'icit_spot' ) ) icit_spot( '115' ); ?>
					</div>
				</div>
			</div>
		</div> -->
	</div>
	<!-- Información Adicional -->
	<!-- <div class="big-picture nostatic box"></div> -->
	

	<div id="panoramic" class="box">
		
		<script>

			var manualControl = false;
			var longitude = 0;
			var latitude = 0;
			var savedX;
			var savedY;
			var savedLongitude;
			var savedLatitude;

			// panoramas background
			var panoramasArray = ["<?php echo get_stylesheet_directory_uri(); ?>/images/panorama-v2.jpg"];
			var panoramaNumber = Math.floor(Math.random()*panoramasArray.length);

			// setting up the renderer
			renderer = new THREE.WebGLRenderer();
			renderer.setSize(window.innerWidth, window.innerHeight);


			document.body.appendChild(renderer.domElement);
			
			// creating a new scene
			var scene = new THREE.Scene();
			
			// adding a camera
			var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
			camera.target = new THREE.Vector3(0, 0, 0);

			// creation of a big sphere geometry
			var sphere = new THREE.SphereGeometry(100, 100, 40);
			sphere.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));

			// creation of the sphere material
			var sphereMaterial = new THREE.MeshBasicMaterial();
			sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])

			// geometry + material = mesh (actual object)
			var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
			scene.add(sphereMesh);

			// listeners
			document.addEventListener("mousedown", onDocumentMouseDown, false);
			document.addEventListener("mousemove", onDocumentMouseMove, false);
			document.addEventListener("mouseup", onDocumentMouseUp, false);
			
			render();
			
			function render(){
				
				requestAnimationFrame(render);
				
				if(!manualControl){
					longitude += 0.1;
				}

				// limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
				latitude = Math.max(-85, Math.min(85, latitude));

				// moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
				camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.cos(THREE.Math.degToRad(longitude));
				camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - latitude));
				camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.sin(THREE.Math.degToRad(longitude));
				camera.lookAt(camera.target);

				// calling again render function
				renderer.render(scene, camera);
				
			}
			
			// when the mouse is pressed, we switch to manual control and save current coordinates
			function onDocumentMouseDown(event){

				event.preventDefault();

				manualControl = true;

				savedX = event.clientX;
				savedY = event.clientY;

				savedLongitude = longitude;
				savedLatitude = latitude;

			}

			// when the mouse moves, if in manual contro we adjust coordinates
			function onDocumentMouseMove(event){

				if(manualControl){
					longitude = (savedX - event.clientX) * 0.1 + savedLongitude;
					latitude = (event.clientY - savedY) * 0.1 + savedLatitude;
				}

			}

			// when the mouse is released, we turn manual control off
			function onDocumentMouseUp(event){

				manualControl = false;

			}
			
			// pressing a key (actually releasing it) changes the texture map
			document.onkeyup = function(event){
				
				panoramaNumber = (panoramaNumber + 1) % panoramasArray.length
				sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])
				
			}
			
		</script>
	</div>



</div>

<div class="instagram-feed box">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="hgroup text-center">
					<h3>Síganos en Instagram</h3>
					<a target="_blank" href="https://instagram.com/jesusplacestore">@jesusplacestore</a>	
				</div>
			</div>
		</div>
		<div class="row">
			<div id="instafeed"></div>
		</div>
	</div>
</div>



<!-- Feed-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/instafeed.min.js"></script>

<script type="text/javascript">
	var feed = new Instafeed({
		get: 'user',
		limit: 10,
		sortBy: 'most-recent',
		resolution: 'standard_resolution',
		userId: '753191431',
		template: '<div class="insta-col"><a target="_blank" href="{{link}}"><img alt="Instagram" src="{{image}}" /></a></div>',
		accessToken: '753191431.1677ed0.5126e1f3fe344156bd0b30c7d1fb2de8'
	});
	feed.run();
	</script>

	<?php get_footer(); ?>
