<?php /* Template Name: Home */ ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header(); ?>


<div class="slider-container box">
	<div class="slider-container-home box">
		<?php echo do_shortcode('[metaslider id=97]'); ?>
	</div>
</div>

<div class="home-container box">
	<div class="categories-container box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="hgroup">
						<div class="title-section box">
							<h2>Bienvenido <br> a nuestra tienda</h2>
							<div class="sep-bar"></div>
							<p class="lead">
								Aquí, podrás encontrarte con Dios, con el susurro de su Espíritu, con la inmensa paz que produce saber que su sabiduría te guía. Bienvenido al lugar donde Dios siempre te sorprenderá.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php echo do_shortcode('[product_categories limit="6" columns="3" ]'); ?>
				</div>
			</div>
		</div>
	</div>
	

	<div class="highlights-products box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="hgroup">
						<div class="title-section box">
							<h2>Nuestros Recomendados</h2>
							<div class="sep-bar"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="hgroup">
						<?php echo do_shortcode('[products limit="8" columns="4" orderby="id" order="DESC" visibility="visible"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>











<?php get_footer(); ?>
