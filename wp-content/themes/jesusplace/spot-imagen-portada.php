<div class="spot spot-icono box">


	<div style="background-image: url(<?php the_post_thumbnail_url();  ?>)" class="imagen-portada nostatic box">

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="hgroup">
						<h1>
							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
								<?php woocommerce_page_title(); ?>
							<?php endif; ?>
						</h1>
						<p class="lead">
							<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
			?>
		</div>
	</div>
</div>
</div>

</div>

<?php // the_content(); ?>
</div>