<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//wc_get_template( 'archive-product.php' );

if (is_product_category( 'jovenes' )){
	wc_get_template( 'archive-jovenes.php' );

}else if (is_product_category( 'biblias' )){
	wc_get_template( 'archive-biblias.php' );

}else if (is_product_category( 'musica' )){
	wc_get_template( 'archive-musica.php' );

}else if (is_product_category( 'peliculas' )){
	wc_get_template( 'archive-peliculas.php' );

}else if (is_product_category( 'libros' )){
	wc_get_template( 'archive-libros.php' );

}else if (is_product_category( 'devocionales' )){
	wc_get_template( 'archive-devocionales.php' );

}else if (is_product_category( 'outlet' )){
	wc_get_template( 'archive-outlet.php' );

}else if (is_product_category( 'pmp' )){
	wc_get_template( 'archive-pmp.php' );

}else if (is_product_category( 'predicas' )){
	wc_get_template( 'archive-predicas.php' );

} else {
	wc_get_template( 'archive-product.php' );
}