<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _tk
 */

get_header(); ?>



<div class="standard-page box">

	<div style="background-image: url(<?php the_post_thumbnail_url();  ?>)" class="hero nostatic box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<div class="hgroup">
						<h1>Uh oh, contenido no encontrado.</h1>
						<p class="lead">
							El contenido que estaba buscando no se ha encontrado, quizás haya cambiado de nombre o 
							dirección web. 
						</p>
						<a href="/" class="btn btn-default">Regresar al Inicio</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="standard-page-content box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

					<?php endwhile; // end of the loop. ?>

				</div>
			</div>
		</div>
	</div>
</div>






<?php get_footer(); ?>