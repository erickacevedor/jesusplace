<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>
<?php

$stylesheet_root = get_stylesheet_directory();
include( $stylesheet_root . '/versionado.php' );

?>

<div class="buy-process box">
	<div class="container">
		<div class="container-inner box">
			<div class="row">
				<div class="col-xs-12 text-center">
					<div class="buy-process-container">
						<h3 class="title-section">Nuestro Proceso de Compra</h3>
						<p class="lead">
							Hemos diseñado un proceso de compra simple, sencillo y muy
							fácil de realizar para que consigas tus productos favoritos
							más rápido y seguro.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-first col-sm-4">
					<div class="col-content box">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/1-w.svg<?=$ver;?>" alt="" class="img-responsive">
						</div>
						<div class="content">
							<h4>Selecciona</h4>
							<p>
								Encuentra el producto ideal para ti.
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-mid col-sm-4">
					<div class="col-content box">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/2-w.svg<?=$ver;?>" alt="" class="img-responsive">
						</div>
						<div class="content">
							<h4>Compra</h4>
							<p>
								Cancela tu producto fácil, rápido y seguro.
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-last col-sm-4">
					<div class="col-content box">
						<div class="image">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/3-w.svg<?=$ver;?>" alt="" class="img-responsive">
						</div>
						<div class="content">
							<h4>Recibe</h4>
							<p>
								Recibe tu producto a domicilio.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="suscribe nostatic box">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="hgroup">
					<h3>No se pierda ninguna novedad</h3>
					<p class="lead">
						Reciba nuestras promociones y descuentos exclusivos directamente <br>
						en su correo electrónico.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<form action="https://jesusplacestore.us17.list-manage.com/subscribe/post" method="POST">
						<input type="hidden" name="u" value="422abe62c1680b20781e1b507">
						<input type="hidden" name="id" value="c390ad4ce8">

						<div id="mergeTable" class="mergeTable">
							<div class="mergeRow dojoDndItem mergeRow-email" id="mergeRow-0">
								<div class="field-content box">
									<input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="">
									<input type="submit" class="formEmailButton" name="submit" value="Suscribirme">
								</div>
							</div>
						</div>
						<input type="hidden" name="ht" value="89b6d78824a639a9deb2bb978a637005fa278b86:MTUxNjk0ODg2NC4wMzU2">
						<input type="hidden" name="mc_signupsource" value="hosted">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




<div id="footer-page" class="footer box">
	<div class="container">
		<div class="row">
			<div class="col-footer col-xs-12 col-sm-3">
				<div class="hgroup">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_white.png" alt="Jesus Place" class="img-responsive logo-footer">
				</div>
			</div>
			<div class="col-footer col-xs-6 col-sm-3">
				<div class="hgroup">
					<h3>Dirección</h3>
					<p>
						Iglesia Cristiana Ríos de Vida. <br>
						Pie de la Popa Calle 30 #29A-11. <br>
						Cartagena, Colombia.
					</p>
					<p>
						Tel: (+57) 301 430 6299
					</p>
				</div>
			</div>
			<div class="col-footer col-xs-6 col-sm-3">
				<div class="hgroup">
					<h3>Información</h3>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-a' ) ); ?>
				</div>
			</div>
			<div class="col-footer col-footer-sm col-xs-12 col-sm-3">
				<div class="hgroup">
					<h3>Síguenos</h3>
					<ul class="list-unstyled social-list">
						<li><a target="_blank" href="https://www.facebook.com/jesusplacestore/"><span class="fa fa-facebook-square"></span></a></li>
						<li><a target="_blank" href="https://instagram.com/jesusplacestore"><span class="fa fa-instagram"></span></a></li>
						<li><a target="_blank" href="https://twitter.com/jesusplacestore"><span class="fa fa-twitter"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="copy text-center box">
	<p class="small">
		&copy; 2018. Jesus Place Store. Todos los derechos reservados.<br>
		Cartagena de Indias, Colombia.
	</p>
	<a target="_blank" title="Diseñado por SIDE Digital :)" href="http://side.digital/?src=jesus">Designed by SIDE Digital</a>
</div>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.min.js<?=$ver;?>"></script>



</body>
</html>
