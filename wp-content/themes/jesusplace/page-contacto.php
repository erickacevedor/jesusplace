<?php /* Template Name: Contacto */ ?>

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _tk
 */

get_header(); ?>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCTh9DTvlShNGL6aKxfJZ9LJ4jyh9JAP8&callback=initialize" async defer></script>
<script>
	function initialize() {
		var myLatLng = {lat: 10.420923, lng: -75.533903};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 16,
			center: myLatLng
		});

		var contentString = '<div id="content">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<h1 id="firstHeading" class="firstHeading">Jesus Place Store</h1>'+
		'<div id="bodyContent">'+
		'<p>Pie de la Popa, Calle 30 #19A-11. Cartagena de Indias, Colombia' +
		'</p>'+
		'</div>'+
		'</div>';

		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Jesus Place Store'
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});









	}
</script>





<div class="standard-page box">

	<div style="background-image: url(<?php the_post_thumbnail_url();  ?>)" class="hero nostatic box">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="hgroup">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="standard-page-content box">
		<div class="box">
			<div class="container containermin">
				<div class="row">
					<div class="col-xs-12 col-sm-5">
						<h3 style="margin-bottom: 20px;">Contáctenos</h3>
						<?php echo do_shortcode('[contact-form-7 id="361" title="Formulario de contacto 1"]');?>
					</div>
					<div class="col-xs-12 col-sm-7">
						<div class="box">
							<h3>Visite nuestra librería</h3>
							<p>
								Estamos ubicados dentro de las instalaciones de 
								la Iglesia Cristiana Familiar Ríos de Vida, ubicada en el barrio Pie de la Popa, Calle 30 #19A-11. Cartagena de Indias, Colombia.
							</p>
							<hr>
							<p><strong>Horarios de atención:</strong></p>
							<p>
								Lunes a Viernes: 8:00AM a 12:00PM y de 1:00PM a 5:00PM. <br>
								Sábados: 9:00AM a 12:00PM. <br>
								Domingos: 7:00AM a 1:00PM y de 5:00PM a 7:00PM.
							</p>
							<hr>
							<p><strong>Números de teléfono</strong></p>
							<p>
								+57 301 4306299<br>
								+575 642 4584<br>
							</p>
							<hr>
							<p>
								<strong>Correo electrónico:</strong>
							</p>
							<a href="mailto:hola@jesusplacestore.com">hola@jesusplacestore.com</a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="map box">
			<div class="container containermax">
				<div class="row">
					<div class="col-xs-12">
						<div id="map"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
