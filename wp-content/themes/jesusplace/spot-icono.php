<div class="spot spot-icono box">
	<!-- Icono -->
	<span class="icono">
		<span class="<?php echo get_post_meta($post->ID, 'icono', true); ?>"></span>
	</span>
    <?php the_content(); ?>
</div>