<?php

//Mostrar 12 productos en el home
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

//Deshabilitar opción tildada de Enviar a una Dirección Diferente
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );


/* Soporte a los nuevos estilos de galería de Woocommerce */
add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}


// Remove product category/tag meta from its original position
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 10 );


add_action( 'woocommerce_after_subcategory_title', 'custom_add_product_description', 12);
function custom_add_product_description ($category) {
	$cat_id        =    $category->term_id;
	$prod_term    =    get_term($cat_id,'product_cat');
	$description=    $prod_term->description;
	echo '<div class="category-description-box box">'.$description.'</div>';
}


// Add class to body depending the category
function woo_custom_taxonomy_in_body_class( $classes ){
	if( is_singular( 'product' ) )
	{
		$custom_terms = get_the_terms(0, 'product_cat');
		if ($custom_terms) {
			foreach ($custom_terms as $custom_term) {
				$classes[] = 'product_cat_' . $custom_term->slug;
			}
		}
	}
	return $classes;
}
add_filter( 'body_class', 'woo_custom_taxonomy_in_body_class' );








// Editar el search box 
add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );

/**
 * woo_custom_product_searchform
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function woo_custom_product_searchform( $form ) {
	
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
	<div class="searchform-content-function">
	<label class="screen-reader-text" for="s">' . __( 'Search for:', 'woocommerce' ) . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Buscar" />
	<input type="submit" id="searchsubmit" value="&#xF002;" />
	<input type="hidden" name="post_type" value="product" />
	</div>
	</form>';
	return $form;
	
}


/* Mostrar sólo tres productos relacionados */ 
function woo_related_products_limit() {
	global $product;
	
	$args['posts_per_page'] = 4;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}


/* Mostrar sólo tres columnas de productos en [TODA] la tienda */ 
add_filter( 'loop_shop_columns', 'wc_loop_shop_columns', 1, 10 );
function wc_loop_shop_columns( $number_columns ) {
	return 4;
}




//Menu 1 Footer 
function register_my_menus() {
	register_nav_menus(
		array(
			'header-top-bar' => __( 'Menú Superior' ),
			'header-nav-bar' => __( 'Menú Navegación' ),
			'footer-menu-a' => __( 'Footer Menu A' ),
			'footer-menu-b' => __( 'Footer Menu B' ),
			'footer-menu-c' => __( 'Footer Menu C' ),
			'footer-menu-legal' => __( 'Footer Legal  ')
		)
	);
}
add_action( 'init', 'register_my_menus' );

add_filter( 'template_include', 'load_custom_post_template' );
function load_custom_post_template( $template ) {
	if ( in_category( 'blog' ) ) {
		$template = STYLESHEETPATH . '/' . 'single-blog.php';
	}
	return $template;
}


/* Custom product template per category */ 

add_filter( 'template_include', 'custom_single_product_template' );
function custom_single_product_template( $template ) {
  if ( is_singular('product') && (has_term( 'jovenes', 'product_cat')) ) {
    $template = get_stylesheet_directory() . '/woocommerce/single-product-jovenes.php';
  } 
  return $template;
}






?>