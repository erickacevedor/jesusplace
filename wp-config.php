<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jesusplace');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?&w%AS$dQgs;)n^dn`Ins.8fiyd%|Y{pR<Emr2!U`%9 b[F/K1cPwBdt|cbx?(U?');
define('SECURE_AUTH_KEY',  'XFE,?~JVi=`QH?8MjZ)KECO#L[o=0tL38#1HOR35Xi.$.Q5WO<1HM4_1}->`V3VG');
define('LOGGED_IN_KEY',    '}Uu kYwLRbj]I_@7{fh31_]8POx3@x4 JHQtsXymaY1b0<2Bm|^r;pN&X{$$pn^w');
define('NONCE_KEY',        'y{0hPqO&32N^@#EDP`hP@i<M]PX30RI%};(hs _U@{+GGZ* H90}#D_ORAarPW .');
define('AUTH_SALT',        'NpXk)u*1AXLo_K%-8J,=NAes{>{0U2l;%JJ;&_EH.:]vrr.j;W$WY&:!YJu*6zl7');
define('SECURE_AUTH_SALT', 'Wa5-^qqvbOJ&;tnfFlUV7!(8Gr^Do<4!bX{F`MN.A5VqOA.n0=*uez^CJ|{B>+=C');
define('LOGGED_IN_SALT',   'r6BH~%RASbqlwq4*n+l:5J3VB9>421j{a1!C=ci13s`GI-nx }FX8O=tMzcdAywh');
define('NONCE_SALT',       '6}U,pj=l}nr=.S;6z)R).TX;zB67FU25_QlA&zVt_B|[zVltk!^]W`8iDiC$gZ(l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
